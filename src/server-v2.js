let http = require('http')
let fs = require('fs')
let url = require('url')

let PORT = 8080;

http.createServer('request', (request, response) => {

        response.writeHead(200, {
            'Content-type': 'text/html; charset=utf-8'
        })
        
        let query = url.parse(request.url, true).query
        let name = query.name

        // Condition Ternaire en remplacement du if/else
        name = (name === undefined || name === '' ) ? "l'anonyme" : name

        response.end("Hey " + name + " !")

}).listen(PORT)