let http = require('http')
let fs = require('fs')
let url = require('url')

http.createServer('request', (request, response) => {

    // recupération du name
    let query = url.parse(request.url, true).query
    let name = query.name
    name = (name === undefined || name === '' ) ? "l'anonyme" : name

    // il y avait un problème d'encodage..
    fs.readFile('./src/index.html', 'utf8', (err, data) => {

        if (err) {
            response.writeHead(404, {
                'Content-type': 'text/html; charset=utf-8'
            })
            response.end("Error 404 : ce fichier n'existe pas")
        }
        else {
            response.writeHead(200, {
                'Content-type': 'text/html; charset=utf-8'
            })

            // remplacer {{ name }} dans le html, par notre variable
            data = data.replace('{{ name }}', name)

            response.end(data)
        }

    })

}).listen(8080)