let http = require('http')
let fs = require('fs')

http.createServer('request', (request, response) => {

    fs.readFile('./src/index.html', (err, data) => {

        if (err) {
            response.writeHead(404, {
                'Content-type': 'text/html; charset=utf-8'
            })
            response.end("Error 404 : ce fichier n'existe pas")
        }
        else {
            response.writeHead(200, {
                'Content-type': 'text/html; charset=utf-8'
            })
            response.end(data)
        }

    })

}).listen(8080)